// eslint-disable-next-line import/no-unassigned-import
import "mocha";

import * as sinonChai from "sinon-chai";
import * as sinon from "sinon";

import * as chai from "chai";
import { ConvertToUrlParams, ReturnOrThrow } from "../../src/Helpers";
import { ApiResult, APIStatusCodes } from "../../src/Models/ApiResult";
import { HttpsError } from "firebase-functions/lib/providers/https";

chai.should();
chai.use(sinonChai);
const assert = chai.assert;

describe("Helpers", () => {
	it("should produce expected encoded string", () => {
		const data = {
			client_id: {item: "3104490bc1c1454a97ebc8212161a444"},
			redirect_uri: {item: "http://localhost/callback"},
			authorization_code: {item: "3104490bc1c1454a97ebc8212161a444"},
			scope: {item: "transactions:read", encode: false},
			input_provider: {item: "uk-santander-oauth2"},
			market: {item: "GB"},
			locale: {item: "en_US"},

		}
		const expectedOut = "client_id=3104490bc1c1454a97ebc8212161a444&redirect_uri=http%3A%2F%2Flocalhost%2Fcallback&authorization_code=3104490bc1c1454a97ebc8212161a444&scope=transactions:read&input_provider=uk-santander-oauth2&market=GB&locale=en_US"

		assert.equal(ConvertToUrlParams(data), expectedOut)
	})

	it("does not encode flagged data", () => {
		const data = {
			scope: {item: "transactions:read", encode: false},
		}
		const expectedOut = "scope=transactions:read"

		assert.equal(ConvertToUrlParams(data), expectedOut)
	})

	it("ReturnOrThrow Should throw if API State missing", () => {
		const spy = sinon.spy(ReturnOrThrow)
		const result = {} as ApiResult;
		try{
		spy(result)}
		catch {}
		spy.threw(new HttpsError("internal", "Unknown API state", result))
	})

	it("ReturnOrThrow Should throw if API State is not success", () => {
		const spy = sinon.spy(ReturnOrThrow)
		const result = {
			APIStatus: APIStatusCodes.ArgumentRejected
		} as ApiResult;
		try{
		spy(result)}
		catch {
		}
		spy.should.have.thrown()
	})

	it("ReturnOrThrow Should return result if API State is success", () => {
		const spy = sinon.spy(ReturnOrThrow)
		const result = {
			APIStatus: APIStatusCodes.Success,
			Data: "string"
		} as ApiResult<string>;
		const actual = spy(result);
		assert.equal(actual, result);
	})
})
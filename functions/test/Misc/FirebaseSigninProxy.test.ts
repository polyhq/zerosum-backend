// eslint-disable-next-line import/no-unassigned-import
import "mocha";

import { FirebaseSigninProxy, FirebaseSigninProxy_Handler } from "../../src/Functions/Misc/FirebaseSigninProxy";
import * as functions from "firebase-functions";

import * as sinonChai from "sinon-chai";
import * as sinon from "sinon";

import * as chai from "chai";
import * as http from "http";

chai.should();
chai.use(sinonChai);
const assert = chai.assert;

describe("FirebaseSigninProxy", () => {
	let handlerSpy: sinon.SinonSpy;
	before(() => {
		handlerSpy = sinon.spy(FirebaseSigninProxy_Handler);
	});

	afterEach(() => {
		handlerSpy.resetHistory(); // reset after each test
	});

	it("should return 400 when query empty", async () => {
		// A fake request object, with req.query.text set to 'input'
		const req = {
			url: "http://localhost:5001/sum-d0c1b/us-central1/Misc-FirebaseSigninProxy",
		} as functions.https.Request;
		// A fake response object, with a stubbed redirect function which does some assertions
		const res = {
			writeHead(statusCode: number) {
				assert.equal(statusCode, 400);
			},
			end(str: string) {
				//No-OP
			},
		} as functions.Response;

		// Invoke FirebaseSigninProxy with our fake request and response objects. This will cause the
		// assertions in the response object to be evaluated.
		await FirebaseSigninProxy(req, res);
	});
	it("handler should throw an augment null error when query empty", () => {
		try {
			handlerSpy(functions.logger, { redirectUrl: undefined });
		} catch {}
		handlerSpy.should.have.thrown();
	});
	it("should return a redirect", async () => {
		// A fake request object, with req.query.text set to 'input'
		const req = {
			url:
				"http://localhost:5001/sum-d0c1b/us-central1/Misc-FirebaseSigninProxy?redirectUrl=exp%3A%2F%2F192.168.1.238%3A19000%2F--%2Fauth%2Fmagic-link",
		} as functions.https.Request;
		// A fake response object, with a stubbed redirect function which does some assertions
		const res = {
			writeHead(statusCode: number, headers: http.OutgoingHttpHeaders) {
				assert.equal(statusCode, 302);
				assert.equal(headers.Location, "exp://192.168.1.238:19000/--/auth/magic-link");
			},
			end(str: string) {
				//No-OP
			},
		} as functions.Response;

		// Invoke FirebaseSigninProxy with our fake request and response objects. This will cause the
		// assertions in the response object to be evaluated.
		await FirebaseSigninProxy(req, res);
	});
	it("handler should return redirect url", () => {
		handlerSpy(functions.logger, { redirectUrl: "exp://192.168.1.238:19000/--/auth/magic-link" });

		handlerSpy.should.have.returned("exp://192.168.1.238:19000/--/auth/magic-link");
	});
});

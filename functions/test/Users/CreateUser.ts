// eslint-disable-next-line import/no-unassigned-import
import "mocha";

import * as sinonChai from "sinon-chai";

import * as chai from "chai";
import { wrap } from "firebase-functions-test/lib/main";
import { ApiResult, APIStatusCodes } from "../../src/Models/ApiResult";
import { CreateUserResponse } from "../../src/Models/DTO/CreateUser";
import { CreateUser } from "../../src/Functions/Users/CreateUser";

chai.should();
chai.use(sinonChai);
//const assert = chai.assert;

describe("CreateUser", () => {
	it("Rejects when arguments are missing", async () => {
		const spy = wrap(CreateUser);
		let data = {}
		const context = {}
		await spy(data, context)
			.then((result: ApiResult<CreateUserResponse>) => {
				result.APIStatus.should.be.returned(APIStatusCodes.ArgumentRejected)
			})

		data = {
			Market: "GB"
		}
		await spy(data, context)
			.then((result: ApiResult<CreateUserResponse>) => {
				result.APIStatus.should.be.returned(APIStatusCodes.ArgumentRejected)
			})

		data = {
			Locale: "en_US"
		}
		await spy(data, context)
			.then((result: ApiResult<CreateUserResponse>) => {
				result.APIStatus.should.be.returned(APIStatusCodes.ArgumentRejected)
			})
	})
})
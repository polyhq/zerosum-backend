import { ApiResult, APIStatusCodes } from "./Models/ApiResult";
import { HttpsError } from "firebase-functions/lib/providers/https";
import { CreateClientAccessToken_Handler } from "./Functions/Users/Support/CreateClientAccessToken";
import { CreateUserAccessToken_Handler } from "./Functions/Users/Support/CreateUserAccessToken";
import * as functions from "firebase-functions";
import { TokenResponseModel } from "./Models/Tink/TokenResponseModel";
import { GetUserGrantCode_Handler } from "./Functions/Users/Support/GetUserGrantCode";

export type EncodingRecord = { item: string | number | boolean; encode?: boolean };

/**
 * Converts an object into a set of URL query string parameters
 * @param object - The object that will be converted
 */
export function ConvertToUrlParams(object: Record<string, EncodingRecord>): string {
	return Object.keys(object)
		.map((key) => {
			object[key].encode = object[key].encode === undefined ? true : object[key].encode;
			if (object[key].encode) {
				return encodeURIComponent(key) + "=" + encodeURIComponent(object[key].item);
			} else {
				return key + "=" + object[key].item;
			}
		})
		.join("&");
}

export async function GetUserTokenWrapper(
	logger: typeof functions.logger,
	tinkUserId: string,
): Promise<ApiResult<TokenResponseModel>> {
	//Acquire a token to perform operations
	const tokenResult = await CreateClientAccessToken_Handler(logger, ["authorization:grant", "user:create"]);
	const token = tokenResult?.Data?.access_token;

	//Check token was retrieved successfully
	if (tokenResult.APIStatus !== APIStatusCodes.Success || !token) {
		return {
			Error: tokenResult.Error,
			HTTPStatus: tokenResult.HTTPStatus,
			APIStatus: tokenResult.APIStatus,
		} as ApiResult<TokenResponseModel>;
	}

	const delegationResult = await GetUserGrantCode_Handler(logger, token, tinkUserId, [
		"transactions:read",
		"accounts:read",
	]);
	const delegationCode = delegationResult?.Data;

	if (delegationResult.APIStatus !== APIStatusCodes.Success || !delegationCode) {
		return {
			Error: delegationResult.Error,
			HTTPStatus: delegationResult.HTTPStatus,
			APIStatus: delegationResult.APIStatus,
		} as ApiResult<TokenResponseModel>;
	}

	return await CreateUserAccessToken_Handler(logger, delegationCode);
}

export function ReturnOrThrow(result: ApiResult<unknown>) {
	switch (result.APIStatus) {
		case APIStatusCodes.UserStateInvalid:
			throw new HttpsError(
				"failed-precondition",
				"The user required for this operation is not in the correct state.",
				result,
			);
		case APIStatusCodes.ArgumentRejected:
			throw new HttpsError(
				"invalid-argument",
				"A supplied argument has been rejected. Check the data you are sending",
				result,
			);
		case APIStatusCodes.Success:
		case APIStatusCodes.NewData:
			return result;
		case APIStatusCodes.GenericError:
			throw new HttpsError("unavailable", "Generic Error", result);
		case APIStatusCodes.ResponseDecodeError:
			throw new HttpsError("internal", "3rd Party Error", result);
		case APIStatusCodes.FetchError:
			throw new HttpsError("unavailable", "3rd Party Error", result);
		case APIStatusCodes.ConfigurationError:
			throw new HttpsError("internal", "Server configuration invalid", result);
		default:
			throw new HttpsError("internal", "Unknown API status code", result);
	}
}

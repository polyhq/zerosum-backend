export type ApiResult<TData = void> = {
	HTTPStatus?: number;
	APIStatus: APIStatusCodes;
	Data?: TData;
	Error?: Error;
};

export enum APIStatusCodes {
	Success = 0,
	GenericError,
	ResponseDecodeError,
	FetchError,
	ConfigurationError,
	ArgumentRejected,
	UserStateInvalid,
	NewData,
}

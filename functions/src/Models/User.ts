export type User = {
	/**
	 * Id of the user in tink
	 */
	TinkId: string;
	/**
	 * The id of the users bank account in tink
	 */
	AccountId: string;
	/**
	 * The id of the credential providing the account in....yes tink
	 */
	CredentialId: string;
	/**
	 * True if the Credentials for the account need refreshing via Tink Link. False otherwise
	 */
	RequiresReAuth: boolean;
};

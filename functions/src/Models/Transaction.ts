export type Transaction = {
	Timestamp: Date;
	Amount: number;
	Description: string;
	Pending: boolean;
	Category: unknown;
	Notes: string;
	TxID: string;
};

export interface TokenResponseModel {
	token_type: string;
	expires_in: number;
	access_token: string;
	scope: string;
	id_hint?: any;
}

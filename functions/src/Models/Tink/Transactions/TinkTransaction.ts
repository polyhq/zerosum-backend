export type PeriodAmount = {
	key: string;
	value: number;
};

export type Query = {
	accounts: string[];
	categories: string[];
	endDate: number;
	externalIds: string[];
	includeUpcoming: boolean;
	limit: number;
	maxAmount: number;
	minAmount: number;
	offset: number;
	order: string;
	queryString: string;
	sort: string;
	startDate: number;
};

export type CurrencyDenominatedAmount = {
	currencyCode: string;
	scale: number;
	unscaledValue: number;
};

export type CurrencyDenominatedOriginalAmount = {
	currencyCode: string;
	scale: number;
	unscaledValue: number;
};

export type PartnerPayload = unknown;

export type Part = {
	amount: number;
	categoryId: string;
	counterpartDescription: string;
	counterpartId: string;
	counterpartTransactionAmount: number;
	counterpartTransactionId: string;
	date: number;
	id: string;
	lastModified: number;
};

export type Payload = unknown;

export type TinkTransaction = {
	accountId: string;
	amount: number;
	categoryId: string;
	categoryType: string;
	currencyDenominatedAmount: CurrencyDenominatedAmount;
	currencyDenominatedOriginalAmount: CurrencyDenominatedOriginalAmount;
	date: number;
	description: string;
	dispensableAmount: number;
	id: string;
	lastModified: number;
	notes: string;
	originalAmount: number;
	originalDate: number;
	originalDescription: string;
	partnerPayload: PartnerPayload;
	parts: Part[];
	payload: Payload;
	pending: boolean;
	timestamp: number;
	type: string;
	upcoming: boolean;
	userId: string;
	userModified: boolean;
};

export type QueryTransactionsRequest = {
	accounts: string[];
	categories: string[];
	endDate: number;
	externalIds: string[];
	includeUpcoming: boolean;
	limit: number;
	maxAmount: number;
	minAmount: number;
	offset: number;
	order: string;
	queryString: string;
	sort: string;
	startDate: number;
};

import { PeriodAmount, Query, TinkTransaction } from "./TinkTransaction";

export type Result = {
	transaction: TinkTransaction;
	type: string;
};

export type QueryTransactionsResponse = {
	count: number;
	metrics: Metrics;
	net: number;
	periodAmounts: PeriodAmount[];
	query: Query;
	results: Result[];
};

export type CATEGORIES = {
	"0e1bade6a7e3459eb794f27b7ba4cea0": number;
};

export type Metrics = {
	AVG: number;
	CATEGORIES: CATEGORIES;
	COUNT: number;
	NET: number;
	SUM: number;
};

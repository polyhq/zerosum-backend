export type GetAccountListResponse = {
	accounts?: Account[];
};

type CurrencyDenominatedBalance = {
	currencyCode: string;
	scale: number;
	unscaledValue: number;
};

type AccountDetails = {
	interest: number;
	nextDayOfTermsChange: string;
	numMonthsBound: number;
	type: string;
};

type TransferDestination = {
	balance: number;
	displayAccountNumber: string;
	displayBankName?: string;
	matchesMultiple: boolean;
	name: string;
	type: string;
	uri: string;
};

export type Account = {
	accountExclusion: string;
	accountNumber: string;
	balance: number;
	closed: boolean;
	credentialsId: string;
	currencyDenominatedBalance: CurrencyDenominatedBalance;
	details: AccountDetails;
	excluded: boolean;
	favored: boolean;
	financialInstitutionId: string;
	flags: string;
	holderName: string;
	id: string;
	identifiers: string;
	name: string;
	ownership: number;
	refreshed: number;
	transferDestinations: TransferDestination[];
	type: string;
};

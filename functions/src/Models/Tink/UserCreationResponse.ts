export interface UserCreationResponse {
	user_id: string;
}

export interface DelegationResponse {
	code: string;
}

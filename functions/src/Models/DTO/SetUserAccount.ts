export type SetUserAccountRequest = {
	AccountId: string;
	CredentialId: string;
};

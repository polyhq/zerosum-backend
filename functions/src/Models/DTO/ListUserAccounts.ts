export type Account = {
	AccountId: string;
	AccountName: string;
	AccountNumber: string;
	HolderName: string;
	Balance: number;
	CurrencyCode: string;
};

export type ListUserAccountsResponse = {
	Accounts: Account[];
};

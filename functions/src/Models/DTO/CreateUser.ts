export interface CreateUserResponse {
	authorization_code: string;
}

export type CreateUserRequest = {
	locale: string;
	market: string;
};

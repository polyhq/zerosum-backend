import * as url from "url";
import * as functions from "firebase-functions";
import { ParsedUrlQuery } from "querystring";

export const FirebaseSigninProxy = functions.https.onRequest((req, res) => {
	const logger = functions.logger;
	logger.info("Parsing url for redirect");
	const { query } = url.parse(req.url, true);
	try {
		const Location = FirebaseSigninProxy_Handler(logger, query);
		res.writeHead(302, { Location });
		res.end(`Redirecting to ${Location}`);
	} catch (e) {
		if (e) {
			res.writeHead(400);
			res.end();
		}
	}
});

export const FirebaseSigninProxy_Handler = (logger: typeof functions.logger, query: ParsedUrlQuery) => {
	const { redirectUrl, ...rest } = query;
	let fixedRedirect: string;
	if (redirectUrl) {
		if (typeof redirectUrl === "string") {
			fixedRedirect = redirectUrl;
			logger.debug("Redirect URL is a string!");
		} else {
			logger.debug("Redirect URL is an array! Converting to string using '/' as separator");
			fixedRedirect = redirectUrl.join("/");
		}
		logger.debug("Parsed Redirect URL: " + redirectUrl);
		const Location = url.format({ pathname: fixedRedirect, query: rest });
		logger.debug(`Redirecting to "${Location}"`);
		return Location;
	} else {
		logger.error("'redirectUrl' query string required");
		throw new Error("Argument Null: redirectUrl");
	}
};

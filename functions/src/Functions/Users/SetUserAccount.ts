import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { ReturnOrThrow } from "../../Helpers";
import { ApiResult, APIStatusCodes } from "../../Models/ApiResult";
import { SetUserAccountRequest } from "../../Models/DTO/SetUserAccount";
import { User } from "../../Models/User";

export const SetUserAccount = functions.https.onCall(async (data: SetUserAccountRequest, context) => {
	const db = admin.firestore();

	const result = await SetUserAccount_Handler(
		functions.logger,
		data.AccountId,
		data.CredentialId,
		context.auth?.uid ?? "",
		db,
	);

	return ReturnOrThrow(result);
});

export const SetUserAccount_Handler = async (
	logger: typeof functions.logger,
	accountId: string,
	credentialId: string,
	uid: string,
	db: FirebaseFirestore.Firestore,
): Promise<ApiResult> => {
	const userDocRef = db.collection("users").doc(uid);
	await userDocRef.set(
		{
			AccountId: accountId,
			CredentialId: credentialId,
			RequiresReAuth: false,
		} as User,
		{ merge: true },
	);

	return { APIStatus: APIStatusCodes.Success };
};

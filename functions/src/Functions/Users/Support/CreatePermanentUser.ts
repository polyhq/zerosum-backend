import * as functions from "firebase-functions";
import { ApiResult, APIStatusCodes } from "../../../Models/ApiResult";
import { UserCreationResponse } from "../../../Models/Tink/UserCreationResponse";
import axios from "axios";

//export const CreatePermanentUser = functions.https.onCall(((data, context) => {
//
//}));

export const CreatePermanentUser_Handler = async (
	logger: typeof functions.logger,
	clientToken: string,
	locale: string,
	market: string,
): Promise<ApiResult<string>> => {
	logger.info("Creating new Tink permanent user");
	let apiResult;
	try {
		apiResult = await axios.post<UserCreationResponse>(
			"https://api.tink.com/api/v1/user/create",
			{
				locale: locale,
				market: market,
			},
			{
				headers: {
					"Content-Type": "application/json; charset=utf-8",
					Authorization: "Bearer " + clientToken,
				},
			},
		);
	} catch (e) {
		logger.error("Fetch error during user creation", e.message);
		return {
			Error: e,
			APIStatus: APIStatusCodes.FetchError,
		};
	}

	if (!apiResult.status.toString().startsWith("2")) {
		logger.error("Non-Success HTTP code when creating Tink user");
		return {
			HTTPStatus: apiResult.status,
			APIStatus: APIStatusCodes.GenericError,
		} as ApiResult<string>;
	}

	try {
		return {
			Data: apiResult.data.user_id,
			APIStatus: APIStatusCodes.Success,
			HTTPStatus: apiResult.status,
		};
	} catch (e) {
		return {
			Error: e,
			HTTPStatus: apiResult.status,
			APIStatus: APIStatusCodes.ResponseDecodeError,
		};
	}
};

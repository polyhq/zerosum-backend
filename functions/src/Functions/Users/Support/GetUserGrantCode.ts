import * as functions from "firebase-functions";
import { ApiResult, APIStatusCodes } from "../../../Models/ApiResult";
import axios from "axios";
import { ConvertToUrlParams } from "../../../Helpers";

export const GetUserGrantCode_Handler = async (
	logger: typeof functions.logger,
	clientToken: string,
	userId: string,
	scope: string[],
): Promise<ApiResult<string>> => {
	let apiResponse;

	try {
		apiResponse = await axios.post(
			"https://api.tink.com/api/v1/oauth/authorization-grant",
			ConvertToUrlParams({
				user_id: { item: userId, encode: true },
				scope: { item: scope.join(), encode: false },
			}),
			{
				headers: {
					"Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
					Authorization: `Bearer ${clientToken}`,
				},
			},
		);
	} catch (e) {
		return {
			Error: e,
			APIStatus: APIStatusCodes.FetchError,
		};
	}

	return {
		Data: apiResponse?.data?.code,
		APIStatus: APIStatusCodes.Success,
	} as ApiResult<string>;
};

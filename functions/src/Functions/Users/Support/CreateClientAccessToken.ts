import * as functions from "firebase-functions";
import { ConvertToUrlParams } from "../../../Helpers";
import { ApiResult, APIStatusCodes } from "../../../Models/ApiResult";
import { TokenResponseModel } from "../../../Models/Tink/TokenResponseModel";
import axios, { AxiosResponse } from "axios";

/*export const CreateClientAccessToken = functions.https.onCall(((data, context) => {

}));*/

export const CreateClientAccessToken_Handler = async (
	logger: typeof functions.logger,
	scopes: string[],
): Promise<ApiResult<TokenResponseModel>> => {
	let apiResult: AxiosResponse<TokenResponseModel>;
	try {
		let clientId;
		let clientSecret;
		try {
			clientId = functions.config()?.tink?.clientid;
			if (clientId === undefined) {
				return {
					APIStatus: APIStatusCodes.ConfigurationError,
				};
			}
		} catch (e) {
			logger.error("Could not read Tink.ClientId from configuration");
			return {
				Error: e,
				APIStatus: APIStatusCodes.ConfigurationError,
			};
		}
		try {
			clientSecret = functions.config()?.tink?.clientsecret;
			if (clientSecret === undefined) {
				return {
					APIStatus: APIStatusCodes.ConfigurationError,
				};
			}
		} catch (e) {
			logger.error("Could not read Tink.ClientSecret from configuration");
			return {
				Error: e,
				APIStatus: APIStatusCodes.ConfigurationError,
			};
		}

		apiResult = await axios.post(
			"https://api.tink.com/api/v1/oauth/token",
			ConvertToUrlParams({
				client_id: { item: clientId, encode: true },
				client_secret: { item: clientSecret, encode: true },
				grant_type: { item: "client_credentials", encode: true },
				scope: { item: scopes.join(), encode: false },
			}),
			{
				headers: {
					"Content-Type": "application/x-www-form-urlencoded",
				},
			},
		);
	} catch (e) {
		return {
			Error: e,
			APIStatus: APIStatusCodes.FetchError,
		};
	}

	if (!apiResult.status.toString().startsWith("2")) {
		return {
			HTTPStatus: apiResult.status,
			APIStatus: APIStatusCodes.GenericError,
		} as ApiResult<TokenResponseModel>;
	}

	try {
		const responseJson = apiResult.data;
		return {
			Data: responseJson,
			APIStatus: APIStatusCodes.Success,
			HTTPStatus: apiResult.status,
		};
	} catch (e) {
		return {
			Error: e,
			HTTPStatus: apiResult.status,
			APIStatus: APIStatusCodes.ResponseDecodeError,
		};
	}
};

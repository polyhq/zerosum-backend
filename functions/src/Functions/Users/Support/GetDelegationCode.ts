import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { ConvertToUrlParams, ReturnOrThrow } from "../../../Helpers";
import { ApiResult, APIStatusCodes } from "../../../Models/ApiResult";
import { DelegationResponse } from "../../../Models/Tink/UserCreationResponse";
import axios from "axios";
import { HttpsError } from "firebase-functions/lib/providers/https";
import { CreateClientAccessToken_Handler } from "./CreateClientAccessToken";
import { CreateUserResponse } from "../../../Models/DTO/CreateUser";
import { User } from "../../../Models/User";

export const GetDelegationCode = functions.https.onCall(async (data, context) => {
	if (context?.auth?.uid === undefined) {
		throw new HttpsError("permission-denied", "Access Denied");
	}

	const tokenResult = await CreateClientAccessToken_Handler(functions.logger, ["authorization:grant"]);
	const token = tokenResult?.Data?.access_token;

	//Check token was retrieved successfully
	if (tokenResult.APIStatus !== APIStatusCodes.Success || !token) {
		return {
			Error: tokenResult.Error,
			HTTPStatus: tokenResult.HTTPStatus,
			APIStatus: tokenResult.APIStatus,
		} as ApiResult<CreateUserResponse>;
	}

	const db = admin.firestore();
	const userDocRef = db.collection("users").doc(context.auth.uid);
	const user = (await userDocRef.get()).data() as User;

	const result = await GetDelegationCode_Handler(functions.logger, token, user.TinkId, data.IdHint, data.Scopes);

	return ReturnOrThrow(result);
});

export const GetDelegationCode_Handler = async (
	logger: typeof functions.logger,
	clientToken: string,
	userId: string,
	idHint: string,
	scopes: string[],
): Promise<ApiResult<string>> => {
	let apiResult;
	try {
		apiResult = await axios.post<DelegationResponse>(
			"https://api.tink.com/api/v1/oauth/authorization-grant/delegate",
			ConvertToUrlParams({
				user_id: { item: userId, encode: true },
				id_hint: { item: idHint, encode: true },
				actor_client_id: { item: "df05e4b379934cd09963197cc855bfe9", encode: false },
				scope: { item: scopes.join(), encode: false },
			}),
			{
				headers: {
					"Content-Type": "application/x-www-form-urlencoded",
					Authorization: "Bearer " + clientToken,
				},
			},
		);
	} catch (e) {
		return {
			Error: e,
			APIStatus: APIStatusCodes.FetchError,
		};
	}

	if (!apiResult.status.toString().startsWith("2")) {
		return {
			HTTPStatus: apiResult.status,
			APIStatus: APIStatusCodes.GenericError,
		} as ApiResult<string>;
	}

	try {
		return {
			Data: apiResult.data.code,
			APIStatus: APIStatusCodes.Success,
			HTTPStatus: apiResult.status,
		};
	} catch (e) {
		return {
			Error: e,
			HTTPStatus: apiResult.status,
			APIStatus: APIStatusCodes.ResponseDecodeError,
		};
	}
};

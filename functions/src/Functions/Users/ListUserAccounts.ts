import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { GetUserTokenWrapper, ReturnOrThrow } from "../../Helpers";
import { ApiResult, APIStatusCodes } from "../../Models/ApiResult";
import { User } from "../../Models/User";
import axios, { AxiosResponse } from "axios";
import { GetAccountListResponse } from "../../Models/Tink/Account";
import { ListUserAccountsResponse, Account } from "../../Models/DTO/ListUserAccounts";

export const ListUserAccounts = functions.https.onCall(async (data, context) => {
	const db = admin.firestore();

	const result = await ListUserAccounts_Handler(functions.logger, context.auth?.uid ?? "", db);

	return ReturnOrThrow(result);
});

export const ListUserAccounts_Handler = async (
	logger: typeof functions.logger,
	uid: string,
	db: FirebaseFirestore.Firestore,
): Promise<ApiResult<ListUserAccountsResponse>> => {
	const userDocRef = db.collection("users").doc(uid);
	const userDoc = (await userDocRef.get()).data() as User;

	const tinkUserId: string = userDoc?.TinkId;

	if (!tinkUserId) {
		return { APIStatus: APIStatusCodes.UserStateInvalid };
	}

	const userTokenResult = await GetUserTokenWrapper(logger, tinkUserId);
	const userToken = userTokenResult.Data?.access_token;
	if (userTokenResult.APIStatus !== APIStatusCodes.Success || !userToken) {
		return {
			Error: userTokenResult.Error,
			HTTPStatus: userTokenResult.HTTPStatus,
			APIStatus: userTokenResult.APIStatus,
		} as ApiResult<ListUserAccountsResponse>;
	}
	let apiResponse: AxiosResponse<GetAccountListResponse>;
	try {
		apiResponse = await axios.get<GetAccountListResponse>("https://api.tink.com/api/v1/accounts/list", {
			headers: {
				Authorization: "Bearer " + userToken,
			},
		});
	} catch (e) {
		return {
			Error: e,
			APIStatus: APIStatusCodes.FetchError,
		};
	}

	if (!apiResponse.status.toString().startsWith("2")) {
		return {
			HTTPStatus: apiResponse.status,
			APIStatus: APIStatusCodes.GenericError,
		} as ApiResult<ListUserAccountsResponse>;
	}

	logger.debug("Before filtering the following acounts were attached", apiResponse.data.accounts)

	const accounts = apiResponse.data.accounts
		?.filter((account) => {
			if (account.closed || account.excluded) {
				return false;
			}

			if (account.accountExclusion.includes("PFM_AND_SEARCH")) {
				return false;
			}

			if (account.accountExclusion.includes("AGGREGATION")) {
				return false;
			}

			return true;
		})
		.map((apiAccount) => {
			return {
				AccountId: apiAccount.id,
				AccountName: apiAccount.name,
				AccountNumber: apiAccount.accountNumber,
				HolderName: apiAccount.holderName,
				Balance: apiAccount.balance,
				CurrencyCode: apiAccount.currencyDenominatedBalance.currencyCode,
			} as Account;
		});

	logger.info("After filtering the following accounts were found.", accounts)

	return {
		APIStatus: APIStatusCodes.Success,
		Data: { Accounts: accounts ?? ([] as Account[]) } as ListUserAccountsResponse,
	};
};

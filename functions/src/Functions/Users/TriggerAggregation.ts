import * as functions from "firebase-functions";
import { ApiResult, APIStatusCodes } from "../../Models/ApiResult";
import * as admin from "firebase-admin";
import axios from "axios";
import { QueryTransactionsResponse } from "../../Models/Tink/Transactions/ApiResponses";
import { Transaction } from "../../Models/Transaction";
import { GetUserTokenWrapper, ReturnOrThrow } from "../../Helpers";
import { TriggerAggregationRequest } from "../../Models/DTO/TriggerAggregation";
import { User } from "../../Models/User";

export const TriggerAggregation = functions
	.runWith({
		timeoutSeconds: 540,
	})
	.https.onCall(async (data: TriggerAggregationRequest, context) => {
		const db = admin.firestore();

		const result = await TriggerAggregation_Handler(functions.logger, data.Qs, context.auth?.uid ?? "", db);

		return ReturnOrThrow(result);
	});

export const TriggerAggregation_Handler = async (
	logger: typeof functions.logger,
	qs: string,
	uid: string,
	db: FirebaseFirestore.Firestore,
): Promise<ApiResult<void>> => {
	//Tests if the query string is accepted by Tink API
	const qsRegex = /^(?:weekdays|weekends|today|yesterday|this (?:month|week|year)|last (?:week|month|year)|week [0-9]+)$/;
	if (!qsRegex.test(qs)) {
		return { APIStatus: APIStatusCodes.ArgumentRejected } as ApiResult;
	}

	const userDocRef = db.collection("users").doc(uid);
	const userDoc = (await userDocRef.get()).data() as User;

	if (!userDoc?.TinkId || !userDoc?.AccountId) {
		logger.error("Tink or Account ID not present");
		return { APIStatus: APIStatusCodes.UserStateInvalid };
	}

	const tinkUserId: string = userDoc?.TinkId;
	const userAccountId: string = userDoc?.AccountId;

	const userTokenResult = await GetUserTokenWrapper(logger, tinkUserId);
	const userToken = userTokenResult.Data?.access_token;
	if (userTokenResult.APIStatus !== APIStatusCodes.Success || !userToken) {
		return {
			Error: userTokenResult.Error,
			HTTPStatus: userTokenResult.HTTPStatus,
			APIStatus: userTokenResult.APIStatus,
		} as ApiResult;
	}

	logger.info("Fetching transaction data from Tink");
	let fetchedCount = 0;
	let totalTransactionHits = 0;
	let newData = false;
	do {
		let apiResponse;
		try {
			apiResponse = await axios.post<QueryTransactionsResponse>(
				"https://api.tink.com/api/v1/search",
				{
					queryString: qs,
					includeUpcoming: true,
					accounts: [userAccountId],
					limit: 15,
					offset: fetchedCount,
				},
				{
					headers: {
						"Content-Type": "application/json",
						Authorization: "Bearer " + userToken,
						Accept: "application/json",
					},
				},
			);
		} catch (e) {
			return {
				Error: e,
				APIStatus: APIStatusCodes.FetchError,
			};
		}

		if (!apiResponse.status.toString().startsWith("2")) {
			return {
				HTTPStatus: apiResponse.status,
				APIStatus: APIStatusCodes.GenericError,
			} as ApiResult;
		}

		totalTransactionHits = apiResponse.data.count;

		//const batch = db.batch();
		for (const transaction of apiResponse.data.results
			.map((result) => result.transaction)
			.map((transactionData) => {
				return {
					Amount: transactionData.amount,
					Notes: transactionData.notes,
					Description: transactionData.description,
					Pending: transactionData.pending,
					Timestamp: new Date(transactionData.date),
					TxID: transactionData.id,
				} as Transaction;
			})) {
			const yearDoc = userDocRef.collection("transactions").doc(transaction.Timestamp.getUTCFullYear().toString());
			await yearDoc.set({}, { merge: true });
			const monthCollection = yearDoc.collection(transaction.Timestamp.getUTCMonth().toString());
			const transactionDoc = monthCollection.doc(transaction.TxID);
			try {
				await transactionDoc.update(transaction);
			} catch (e) {
				//logger.debug("Update failed, attempting set operation");
				try {
					await transactionDoc.set(transaction, { merge: true });
					newData = true;
				} catch (e2) {
					logger.error("Error adding document to collection", e2);
				}
			}
			//batch.set(transactionDoc, transaction)
		}
		//await batch.commit();
		fetchedCount += apiResponse.data.results.length;
	} while (fetchedCount < totalTransactionHits);

	logger.info("Fetched " + fetchedCount + " from Tink. New data: " + newData);

	return {
		APIStatus: newData ? APIStatusCodes.NewData : APIStatusCodes.Success,
	} as ApiResult;
};

import * as functions from "firebase-functions";
import { ApiResult, APIStatusCodes } from "../../Models/ApiResult";
import { CreateUserRequest, CreateUserResponse } from "../../Models/DTO/CreateUser";
import { CreateClientAccessToken_Handler } from "./Support/CreateClientAccessToken";
import { CreatePermanentUser_Handler } from "./Support/CreatePermanentUser";
import { GetDelegationCode_Handler } from "./Support/GetDelegationCode";
import * as admin from "firebase-admin";
import { HttpsError } from "firebase-functions/lib/providers/https";
import { ReturnOrThrow } from "../../Helpers";
import { User } from "../../Models/User";

export const CreateUser = functions.https.onCall(async (data: CreateUserRequest, context) => {

	if(context.auth?.uid === undefined){
		throw new HttpsError("permission-denied", "Access Denied")
	}

	if (data.locale === undefined) {
		throw new HttpsError("invalid-argument", "Missing 'locale' argument");
	}

	if (data.market === undefined) {
		throw new HttpsError("invalid-argument", "Missing 'market' argument");
	}

	const db = admin.firestore();

	const result = await CreateUser_Handler(functions.logger, data.locale, data.market, context.auth.uid, db, context.auth.token.email!);

	return ReturnOrThrow(result);
});

export const CreateUser_Handler = async (
	logger: typeof functions.logger,
	locale: string,
	market: string,
	uid: string,
	db: FirebaseFirestore.Firestore,
	idHint: string
): Promise<ApiResult<CreateUserResponse>> => {
	//Acquire a token to perform operations
	const tokenResult = await CreateClientAccessToken_Handler(logger, ["authorization:grant", "user:create"]);
	const token = tokenResult?.Data?.access_token;

	//Check token was retrieved successfully
	if (tokenResult.APIStatus !== APIStatusCodes.Success || !token) {
		return {
			Error: tokenResult.Error,
			HTTPStatus: tokenResult.HTTPStatus,
			APIStatus: tokenResult.APIStatus,
		} as ApiResult<CreateUserResponse>;
	}

	const userIdResult = await CreatePermanentUser_Handler(logger, token, locale, market);
	const tinkUserId = userIdResult?.Data;

	if (userIdResult.APIStatus !== APIStatusCodes.Success || !tinkUserId) {
		return {
			Error: userIdResult.Error?.message,
			HTTPStatus: userIdResult.HTTPStatus,
			APIStatus: userIdResult.APIStatus,
		} as ApiResult<CreateUserResponse>;
	}

	const delegationResult = await GetDelegationCode_Handler(logger, token, tinkUserId, idHint, [
		"credentials:read",
		"credentials:refresh",
		"credentials:write",
		"providers:read",
		"user:read",
		"authorization:read",
	]);
	const delegationCode = delegationResult?.Data;

	if (delegationResult.APIStatus !== APIStatusCodes.Success || !delegationCode) {
		return {
			Error: delegationResult.Error,
			HTTPStatus: delegationResult.HTTPStatus,
			APIStatus: delegationResult.APIStatus,
		} as ApiResult<CreateUserResponse>;
	}

	const userDocRef = db.collection("users").doc(uid);

	await userDocRef.set({
		TinkId: tinkUserId,
	} as User);

	return {
		APIStatus: APIStatusCodes.Success,
		Data: {
			authorization_code: delegationCode,
		},
	} as ApiResult<CreateUserResponse>;
};

import * as admin from "firebase-admin";

admin.initializeApp();

exports.Misc = require("./Functions/Misc/index");
exports.Users = require("./Functions/Users/index");
